
import numpy as np
import copy
import networkx as nx

from itertools import cycle
from matplotlib import pyplot as plt

import logging
import logging.handlers
from plyfile import PlyData

from mainutils import *
from geomutils import *
from geom3d import *

#from polyhedron import *

#from polygon import Polygon
#import polygon


logfile = open("geomutils.log", "w+")

logging.basicConfig(stream=logfile, level=logging.DEBUG)
log = logging.getLogger('geomutils')
log.setLevel(logging.DEBUG)

#TODO: handler powoduje, ze @log_with zle dziala
#handler = logging.handlers.RotatingFileHandler(
#              "geomutils.log", maxBytes=10000, backupCount=5)

#log.addHandler(handler)



#log.info('prism module')


#TODO: Maybe add Sympy numbers or points for symbolic computation?
#Is this possible?

#np.array is treated as Point3d
# class Point3d:
#     #Not use this, use np.array

#     @signature(Any, np.array)
#     def __init__(self, point):
#         """ Initialize 3d point.
#             :param point: point coordinates as np.ndarray, list or tuple
#         """
#         self.point = point



class Polyhedron:
    """ 3D Polyhedron class.
        Represents full polyhedron.
    """
    # def __init__(vertices, nbVertices):
    #     """ Initialization of Polyhedron.
    #         :param vertices: list of n vertices position (nd.arrays)
    #         :param nbVertices: list of n list,
    #             each sublist contains list of vertices indices, to this vertex is linked with edge.
    #     """

    # def __init__(self):
    #     pass

    #tetrahedron.ply - from https://github.com/dranjan/python-plyfile/blob/master/examples/tet.ply

    def __init__(self, graph=None):
        """ Initialize polyhedron. 
            :param graph: NetworkX graph containing position of vertex as node attribute.
        """
        if graph!=None:
            raise Exception("graph to polyhedron - not implemented")

    def __str__(self):
        if self.plydata!=None:
            return "Polyhedron(v={})".format(self.vertices)
        else:
            return "Polyhedron: {}".format(id(self))

    @staticmethod
    def fromPlyFile(path):
        ph = Polyhedron()
        ph.plydata = PlyData.read(open(path))
        return ph

    def toPlyFile(self, path):
        self.plydata.write(open(path, "w"))

    def getPlydata(self):
        return self.plydata

    @property
    def vertices(self):
        """ Return list of vertices positions.
            :returns: list of coordinate tuples, list or np.arrays
        """
        #TODO: maybe create PolyhedronPLY(Polyhedron)?
        v = (self.plydata['vertex'])
        verts = map(lambda x: (x[0],x[1],x[2]),v)
        print verts
        return np.array(verts)

    #W przypadku property trudno jest z aktualizacja elementu kolekcji lub jakiegos pola
    #Bo property czesto moze przekazywac jakas kopie?

    def getVertex(self, i):
        return self.vertices[i]

    def setVertex(self, i, value):
        self.plydata['vertex'][i] = value

    def getPolygons(self):
        """ Return list of all faces as 3D polygons.
        """
        polygons = []
        for face in self.plydata['face'].data:
            face = face[0] #Get face vertices from face data
            print face
            polygonVertices = map(lambda x: self.vertices[x], face)
            polygons.append(Polygon3d(polygonVertices))

        return polygons

    @contract(vector='array[3]')
    def move(self, vector):
        """ Move polyhedron by vector. Mutate polyhedron.
        :param vector: vector as np.array
        :return:
        """
        #FIXME: it not works?
        print(type(self.vertices))
        for i in range(len(self.vertices)):
            #print vertex, vector
            #print type(vertex)
            #self.vertices[i] = (vector + (self.vertices[i])).tolist()
            self.setVertex(i, (vector + (self.vertices[i])))

    def moved(self, vector):
        ph2 = copy.deepcopy(self)
        ph2.move(vector)
        return ph2

    def reflect(self):
        """ Reflect polygon in plane. Now only yz plane.
        """
        for i in range(len(self.vertices)):
            vertices = self.vertices
            vertices[i][1] = -vertices[i][1]
            self.setVertex(i, vertices[i])

    #TODO: rotate?

    # def faces():
    #     pass


    def plot(self, plotType="surfaces", plotter=None, show=False):
        """ Plot Polyhedron.
            :param plotType: "points", "lines", "surfaces"
        """
        kwargs = {}

        print "Plot polyhedron"

        #raise "Not use this method. Plot in Mayavi/"
        #Should I use this method?

        #if plotType=="points":
        #    plotCrd = np.array(self.getVertices()).T
        #    scatter3d(*plotCrd)

        # if plotter==None:
        #     plotter = plt

        #elif plotType=="surfaces":
        for polygon in self.getPolygons():
            polygon.plot(show=show, plotter=plotter, **kwargs)

        #else:
        #    raise "Not supported type"

        if show==True: plt.show()

    def plotMayavi(self):
        """ Plot Polyhedron in Mayavi.
        """
        for polygon in self.getPolygons():
            polygon.plotMayavi()


    # def inside(point):
    #     pass

    # def equals(polyhedron):
    #     """ Return true if this polyhedron equals another polyhedron (as volume).
    #         Polyhedron are equals if they have identical vertices positions and edge positions.
    #         Numbering of vertices and edges not matters.
    #     #>>> ph1.equals(ph2)
    #     #>>> ph2.equals(ph1)
    #     """
    #     pass

    # def intersection(polyhedron):
    #     """ Compute intersection of two polyhedrons.

    #         :returns: intersection geometry
    #     """
    #     return polyhedronsIntersection(self, polyhedron)


        #TODO: intersection with other objects
        #TODO: Intersection should be simplest geometry object which is possible 
        #   (e.g. polygon, not set of lines)

    #def volume():

#We will use plyfile library

class EdgeDCEL:
    @autoassign
    def __init__(self, next, twin):
        pass

    @property
    def next(self):
        return self.next

    @property
    def twin(self):
        return self.twin

class UniversalNumberReader:
    #TODO: read element to named struct
    #reader must ignore blank lines if not specified differently in parameters

    @staticmethod
    def readElementLine(stream, elementSize):
        """ Read row from input file. @see readDataGroup.
            Return empty list if empty row.
            Return None if end of file.
        """
        line = next(stream)

        #end of file
        if line==None:
            element = None

        row = line.split()

        #empty line
        if len(row)==0:
            element = []
        else:
            #line with numbers
            element = map(float, row)
            if (elementSize != None) and (len(element) != elementSize):
                raise Exception("Wrong number count in line: '{0}'".format(element))

        return element

    @staticmethod
    def readDataGroup(stream, elementSize, lines=None):
        """ Read array of numbers from input stream.
            Default type is float.
            Each row form one element. Numbers in row are separated by space.
            Group end with blank line or after "lines" lines, when "lines" parameter is specified.
            :param stream: text stream of data
            :param elementSize: count of numbers in one row; if 0, elements count is not checked
            :param lines: count of lines; if None, group end with blank line
            :returns: list of row (each row is list of numbers)
        """   
        elements = []




        if lines==None:
            #end when empty line
            #TODO: test

            groupEnd = False
            while not groupEnd:
                element = UniversalNumberReader.readElementLine(stream, elementSize)
                if element in ([], None):
                    groupEnd = True

                elements.append(element)

        else:
            for lineCount in xrange(lines):
                element = UniversalNumberReader.readElementLine(stream, elementSize)

                while (element==[]):
                    element = UniversalNumberReader.readElementLine(stream, elementSize)

                if element==None:
                    raise Exception("end of file before group end")

                elements.append(element)

        return elements

#TODO: maybe example file format to another place?
class PolyhedronReader:
    @staticmethod
    def readAsGraph(filename, format=1):
        """ Import polyhedron from file and store as NetworkX graph.
            Edges are undirected.

            :param format: format kind
            :returns: NetworkX graph, nodes(vertices) have position attribute

            File format 1 (on example):
            5 5 - number of vertices (n), number of faces (m); numeration start from 0
                - one blank line; next vertices positions in format (x y z); order is from 0 to n
            0 0 0
            1 0 0
            1 1 0
            0 1 0
            0.5 0.5 1
                - one blank line; next faces as cycle of vertices, they contain (from face we can get edges); 
                        order of vertices may be "clockwise" or "counterclockwise"
            0 1 2 3 4
            0 1 4
            1 2 4
            2 3 4
            3 0 4
                

            >>> ph = PolyhedronReader.readAsGraph("tests/polyhedron.dat")
            >>> ph.number_of_edges()
            8
            >>> ph.number_of_nodes()
            5
            >>> positions = nx.get_node_attributes(ph, 'position')
            >>> positions = map(tuple, positions.values())
            >>> (1, 1, 0) in positions
            True
        """

        Reader = UniversalNumberReader
        stream = open(filename, "r")

        #read structured data from file
        vertexCount, faceCount = map(int, tuple(Reader.readElementLine(stream, 2)))

        vertices = Reader.readDataGroup(stream, 3, lines=vertexCount)
        faces = Reader.readDataGroup(stream, None, lines=faceCount)

        #create graph from data
        graph = nx.Graph()
        #graph.add_nodes_from(vertices)
        for index, position in zip(range(vertexCount), vertices):
            graph.add_node(index, position=position)

        #graph.add_edges_from(edges)

        for face, ind in zip(faces, range(0, len(faces))):

            #Cycle through each edge forming this face
            for vertex1, vertex2 in cyclePairwise(face):
                #edge will contain index of face
                try:
                    ind2 = graph[vertex1][vertex2]["face"]
                    #update edge
                    graph.add_edge(vertex1, vertex2, face=[ind2, ind])
                except Exception:
                    #create edge
                    graph.add_edge(vertex1, vertex2, face=[ind])

        return graph


# class PolyhedronDCEL:
#     def __init__(self, graph, vertexPositions):
#         """ Create DCEL from undirected graph of polyhedron and vertex positions.
#             :param graph: graph (as NetworkX graph)
#             :param vertexPositions: positions of vertex (as list of np.ndarray)
#         """

#     def getPoints(self):
#         pass

#     def getLines(self):
#         pass

#     def getPolygons(self):
#         pass




class Line3d:

    #@signature(Any, np.array, np.array, np.array)
    def __init__(self, point, direction=None, point2=None):
        """ Create line in 3d space.
            Create it from point and (direction or second point).
            Points and direction should be nd.arrays.
        """
        self.point = point
        #direction may now be not normalized

        if direction!=None:
            self.direction = np.array(direction)
        elif point2!=None:
            self.direction = np.array(point2) - np.array(point)
        else:
            raise Exception("Wrong arguments") 

        return

    def __repr__(self):
        return "Line3d(point={0}, direction={1})".format(self.point, self.direction)


class Plane:
    """ Plane in 3d space.
    """
    #Plane cannot be changed now (not needed?).
    #Is there use case, when normal vector must be not normalized?
    #Then we can add another field for this value

    @autoassign
    def __init__(self, planePoint, nvNormalized):
        """ Initialize plane.
        """
        pass

    def __repr__(self):
        return "Plane3d(planePoint={0}, normalVector={1})".format(self.planePoint, self.nvNormalized)

    #@log_with()
    def _planeLineIntersection(self, line3d):
        """ Compute intersection of plane with line.
            Line is identified by point on line and direction vector.
        """
        #log.info((self, line3d))

        #https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
        dot1 = np.dot(self.planePoint - line3d.point, self.nvNormalized)
        dot2 = np.dot(line3d.direction, self.nvNormalized) #tak wlasnie ma byc

        if dot2 == 0:
            #degenerate case
            if dot1 == 0:
                log.info("Degenerate - line on plane")
                return line3d
            else:
                log.info("Degenerate - parallel line outside plane")
                return emptyGeometry


        d = dot1 / dot2

        p = d * line3d.direction + line3d.point

        return p


    #@log_with()
    def _planeLineSegmentIntersection(self, line):
        """ Compute intersection of plane with 3D line segment.
            :param line: LineSegment3D
        """
        assert(isinstance(line, LineSegment3D))
        #log.info((self, line))

        #project vectors to nvNormalized (nvNormalized is normalized)
        r1 = np.dot(self.nvNormalized, line.start - self.planePoint)
        r2 = np.dot(self.nvNormalized, line.end - self.planePoint)

        
        if (((r1>0) and (r2>0)) or ((r1<0) and (r2<0))):
            #TODO: what if on line?
            result = emptyGeometry

        elif (r1==0):
            #result = Point3d(line.start)
            result = line.start

        elif (r2==0):
            result = line.end

        else:
            result = self._planeLineIntersection(Line3d(line.start, line.move()))


        #log.info(result)
        return result

    #@log_with()
    def intersection(self, geometryObject):
        #log.info((self, geometryObject))
        #TODO: unit tests
        #maybe dispatch?

        if isinstance(geometryObject, LineSegment3D):
            return self._planeLineSegmentIntersection(geometryObject)


        elif isinstance(geometryObject, Polygon3d):
            #Only convex polygon by now
            pol = geometryObject

            result = filter(lambda x: not isinstance(x, EmptyGeometry),[self.intersection(line) for line in pol.edges])
            #result should have now maximum elements, because polygon is convex            
            #raise Exception(str(result))

            if len(result)==0:
                return emptyGeometry

            elif len(result)==1:
                log.info("degenerate case - polygon tangent with plane")
                return result[0]

            else:
                return LineSegment3D(result[0], result[1])


        else:
            raise "intersection not defined for this type"



#Should this class be renamed to ConvexPolygon3D?

class Polygon3DFactory:
    # @staticmethod
    # def square(center=None, normalVector=None, edgeLength=1):
    #     #rotationAngle=0.0
    #     pass

    # def rectangle(center=None, normalVector=None, edgeLength=(2,1))

    pass


#TODO: file geom3d

class Polygon3d:
    """ Polygon3d class.
        >>> pol1 = Polygon3d([[0, 0, 0],[0, 1, 0], [1, 1, 0], [1, 0, 0]])
        >>> pol2 = Polygon3d([[0, 1, 0], [1, 1, 0], [1, 0, 0], [0, 0, 0]])
        >>> pol3 = Polygon3d([[0, 2, 0], [1, 1, 0], [1, 0, 0], [0, 0, 0]])
        >>> pol1 == pol2
        True
        >>> pol1 == pol3
        False
        >>> pol1 == emptyGeometry
        False
        >>> len(set([pol1, pol2]))
        1
        >>> pol4 = Polygon3d([[0,-1,0.5],[0,-1,1.5],[0,1,1.5],[0,1,0.5]])
        >>> pol5 = Polygon3d([[1,0,0],[-1,0,0],[-1,0,1],[1,0,1]])
        >>> pol6 = Polygon3d([[1,0.1,0],[-1, 0.05, 0],[-1,0.2,1],[1,0.05,1]]) #Maybe simpler example?
        >>> pol4.doIntersects(pol5)
        True
        >>> pol5.doIntersects(pol6)
        False
        >>> pol4.doIntersects(pol6)
        True
        >>> line = pol4.intersection(pol5)
        >>> isinstance(line, LineSegment3D) #Is this needed?
        True
        >>> set([tuple(line.start), tuple(line.end)]) == set([(0,0,0.5),(0,0,1.5)])
        True
        >>> geom = pol5.intersection(pol6)
        >>> isinstance(geom, EmptyGeometry)
        True
        >>> geom == emptyGeometry
        True
    """
    #It seems to work

    #@log_with()
    #@log_dec
    def __init__(self, vertices):
        """ Initialize 3D convex polygon.
            Polygon is full (needed for intersection etc.).
            Throw exception if vertices are not planar (with some epsilon).
            :param vertices: list of vertices from boundary chain (3D points);
                list must be in good order
        """
        #print (len(vertices[0]))
        #assert(len(vertices[0])==3) #3D vertices

        self.vertices = map(np.array, vertices)

        pass

    def __repr__(self):
        return "Polygon3d({0})".format(self.vertices)

    #Should polygon be immutable?
    #This will make cached values possible
    #Or maybe invalidate properties after changing state of object?

    # def normalVector(self):
    #     """ Returns normal vector of plane containing
    #     """

    def __eq__(self, polygon2):
        """ Polygon3d are equal if they are same geometry set (order of segments is not important).
            Polygons are convex, so vertices order is not important.
        """
        log.info("equality comparision: {0}, {1}".format(self, polygon2))
        #what with orientability?; not needed for our use case 
        result =  (isinstance(polygon2, self.__class__) and \
            set(asListOfTuples(self.vertices)) == set(asListOfTuples(polygon2.vertices)))

        log.info("result: {0}".format(result))
        return result

    #@log_with()
    def __hash__(self):
        log.info(self)
        listOfTuples = asListOfTuples(self.vertices)
        return hash(frozenset(listOfTuples))

    def __ne__(self, other):
        return not self.__eq__(other)

    def triangulate(self):
        """ Triangulate 3d polygon.
            Works only on convex polygons.
            Not run it for nonconvex polygons.
        """
        #It works good
        triangles = []

        v1, v2 = 0, 1 #self.vertices[0:2]
        for vertex in xrange(2, len(self.vertices)):  #self.vertices[2:]:
            v3 = vertex
            triangles.append((v1, v2, v3))
            v1 = 0 #v2
            v2 = v3

        return triangles


    def moved(self, vector):
        """ Return new moved Polygon3D.
            Order of vertices should be preserved (only moved by vector).
            :param vector: vector to move polygon (nd.array)
        """
        #If order of vertices will be not preserved, we will need VertexChain class
        return Polygon3d([vertex + vector  for vertex in self.getVertices()])


    def plot(self, plotType="surfaces", plotter=None, color=None, show=False, **kwargs):
        """ Plot Polygon3d.
            :param plotType: "points", "lines", "surfaces"
        """
        #Should I use plotMayavi?
        #kwargs = {}

        #TODO: plotter class?
        vertices = self.getVertices()
        #vertices.append(self.getVertices()[0])

        plotCrd = np.array(vertices).T.tolist()

        if plotter==None:
            plot3d(*plotCrd)
        else:
            plotter.plot3d(*plotCrd, plotType="surface", color=color, polygon=self) #, **kwargs)

        if show: plt.show()

        # if plotType=="points":
        #     scatter3d(*plotCrd)
        #
        # elif plotType=="lines":
        #     plot3d(*plotCrd)
        #
        # else:
        #     log.info("Not supported type, other selected.")
        #     plot3d(*plotCrd)


    def plotMayavi(self, **kwargs):
        coords = np.array(self.getVertices()).T
        triangles = [(0, 1, 2)]
        #TODO: now plots only first triangle of polygon
        #  needed triangulation of polygon

        s = mlab.triangular_mesh(coords[0], coords[1], coords[2], triangles, opacity=0.7, **kwargs)



    @property
    def verticesCycle(self):
        return Indexable(cycle(self.vertices))

    def getVertices(self):
        return self.vertices

    @Lazy
    def edges(self):
        """ Polygon edge list.
        """
        return [LineSegment3D(v1, v2) for v1, v2 in cyclePairwise(self.vertices)]

    def _getEdgeVector(self, edgeIndex):
        """ Return edge as free vector (from start to end).
            :param edgeIndex:nr of edge, starting from vertex nr 0
        """
        return self.verticesCycle[edgeIndex+1] - self.verticesCycle[edgeIndex]


    def getPlane(self):
        """ Return Plane containing this polygon.
            :returns: Plane object.
        """

        normalVector = normalize(np.cross(self._getEdgeVector(1), self._getEdgeVector(0)))
        planePoint = self.vertices[0]

        return Plane(planePoint, normalVector)


    def doIntersects(self, polygon3d):
        """ Check if two convex polygons intersects.
            :returns: true if intersection is not empty, false otherwise.
        """
        #TODO: move tests to unittest module
        #TODO: implement without calculating intersection, if needed
        #if (self.intersection() != emptyGeometry)
        if (self.intersection(polygon3d) == emptyGeometry):
            #Every empty geometry set should be equal to emptyGeometry
            return False
        else:
            return True
        #Checking if two polygons intersects
        pass



    #@log_with()
    def intersection(self, polygon3d):
        """ Compute intersection of two convex 3D polygons.
            :param polygon3D: 3D polygon as Polygon3D
            :returns: intersection as geometry (Point, LineSegment3D, Polygon3D) or sum of foregoing.
        """
        #log.info(("Polygon3d intersection", self, polygon3d))
        (plane1, plane2) = self.getPlane(), polygon3d.getPlane()

        #plane contains poligon
        #plane intersection with geomObj contains polygon intersection with geomObj
        inter1 = plane1.intersection(polygon3d)
        inter2 = plane2.intersection(self)

        if emptyGeometry in (inter1, inter2):
            return emptyGeometry

        #TODO: degenerate case - one point

        #inter1.intersection(inter2)
        result = inter1.sameLineIntersection(inter2) #intersection with line segment on same line
        #TODO: what if point or empty geometry?
        #[2,1,1][2,1,0.5] [2,2,0.5][1,2,0.5] [1,2,1][1,2,0.5], [1,1,1][2,1,1] [1,2,1][1,1,1] [2,1,0.5][2,2,0.5]
        return result



    def contains(self, geometry):
        """ Check if this 3D polygon contains some geometrical set.
            :returns: True if this poligon contains geometry, False otherwise
        """
        pass
        #This method may be needed for testing?
        #Or can i test 3d polygon without this method?


class Prism(Polyhedron):
    def __init__(self, basePolygon, height, position, rotation, relativePositive="Vertex"):
        """ Prism (polyhedron) class.
        """

    # def getFaces(self):
    #     """ Return prism faces as polygon list.
    #     """

    #     faces = []

    #     base1 = basePolygon
    #     base2 = basePolygon + height



#class ListBuilder
#maybe yield?


class Cuboid(Prism):
    """ Cuboid class with faces and vertices.
        Implemented methods: getPlanes

        >>> ();c = Cuboid([2, 3, 4]);() # doctest:+ELLIPSIS
        (...)
        >>> len(list(c.getPlanes()))
        6
        >>> 

        #>>> c.inside([1, 2, 3])
        #True
        #>>> c.inside([3, 0, 0])
    """
    #TODO: equality not work as expected or CuboidFactory is broken

    #TODO: maybe create cuboid by adding tetrahedrons, similar to CGAL example?
    #TODO: maybe position and rotation should be in class GeometryObject
    #Maybe "relativePos" should be "center" or something similar?

    def __init__(self, dimensions, position=[0,0,0], rotation=None, relativePos="firstVertex"):
        """ Initialize cuboid class.
            Cuboid now is parallel to x,y,z axes.
            :param dimensions: vector of dimensions of cuboid
            :param position: position of cuboid, 
                relative to item specified in relativePos
            :param rotation: rotation of cuboid (not implemented) (around center of cuboid)
            :param relativePos: name of element to which position is specified
                "firstVertex" - vertex with lowest values (not counting rotation)
        """
        #TODO: implement rotation

        #super(Polygon3d(...), position, rotation)

        #FIXME: position
        self.createTopology(dimensions, position)

        #self.move(position)



        # for faceInd in facesInd:
        #     for i in range(0, 4):
        #         #start with 0
        #         faceInd[i] -= 1 #will it work?

        self.__initPlanes()

    def __repr__(self):
        #How to represent polyhedron?
        polygonStr = ",\n".join(map(str, self.getPolygons()))
        return "Polyhedron(polygons=\n{0}\n)".format(polygonStr)

    #@log_with()
    def __eq__(self, cuboid2):
        log.info((self, cuboid2))
        polygons1 = set(self.getPolygons())
        polygons2 = set(cuboid2.getPolygons())

        #Polyhedrons are equal if they have same set of faces.
        result = (polygons1 == polygons2)
        

        log.info(result)
        return result


    def __ne__(self, other):
        return not self.__eq__(other)

    #def move(self):

    def planesFromFaces(self):
        for face in self.faces:
            yield face.getPlane()

    def __initPlanes(self):   
        """ Initialize plane list.
            Plane list contains plane for each face of polyhedron.
        """
        self.planes = self.planesFromFaces()

    def getVertices(self):
        return self.vertices

    def getPlanes(self):
        """ Return planes of cuboid.
            :returns: sympy.Plane - list of planes of cuboid; plane center should be in cuboid face center
        """
        return self.planes

    def getPolygons(self):
        #getFaces?
        return self.faces

    # def inside(self, point):
    #     """ Returns if point is inside
    #         :returns: True if inside.
    #             Else returns list (or some structure) of planes which give outside result.
    #     """
    #     #TODO: refactor, change name?
    #     outside = []

    #     for plane in self.planes:
    #         #if plane.angle_between(point) < PI:
    #         #   return plane
    #         (center, normalVec) = plane
    #         p2 = point - center #change coordinates
    #         np.dot(normalVec, p2)
    #         print (normalVec, p2)
    #         if (np.dot < 0):
    #             outside.append(plane)

    #     if len(outside)==0:
    #         return True
    #     else:
    #         #FIXME: problem when 2 or more planes will be returned
    #         return outside

    #Prosciej jest korzystac z gotowych bibliotek, bo maja okreslony interfejs

    def prismBasesToStructure(self, lowBase, height):
        """ Create vertices and faces for prism data.
            :param lowBase: low base of prism as polygon
            :param height: height of prism
            :returns: pair - (vertices, faces), 
                faces - list of polygons
        """
        vertices = []
        faces = []


        highBase = lowBase.moved([0,0,height])

        #Create vertices of cuboid and create two faces of cuboid(bases)
        vertices.extend(lowBase.getVertices())
        faces.append(lowBase)

        vertices.extend(highBase.getVertices())
        faces.append(highBase)



        #Vertical edges
        columnEdges = zip(lowBase.getVertices(), highBase.getVertices())
        columnEdgePairs = cyclePairwise(columnEdges)

        #Add vertical faces of cuboid (prism)
        for (firstColumn, secondColumn) in columnEdgePairs:
            #Reversed - because we need good order of vertices
            polygonVertices = list(firstColumn) + list(reversed(secondColumn))

            faces.append(Polygon3d(polygonVertices))        


        #ascertain that all vertices are nd.arrays
        vertices = map(np.array, vertices)

        #print "Polygon3d: {}".format(faces)
        return (vertices, faces)

    def createTopology(self, dimensions, position):
        """ Create cuboid vertices (in good order (consistent with faces)?).
            Create cuboid faces.
            Return cuboid vertices as list of numpy.arrays.
            Why is dimensions needed for creating topology?
            :param dimensions: dimensions of cuboid (from start)
            :param position: position of cuboid (of the lowest vertex)

            Vertices indexes:
            Front:
            3 2
            0 1
            Back:
            7 6
            4 5
        """
        #TODO: should faces be polygons or should they link to vertices by indexes?
        #TODO: maybe decouple topology and vertices positions?

        #TODO: maybe Double Connected Edge List?

        #Create bases chains (treat cuboid as prism)
        lowBase = Polygon3d([[0, 0, 0], [dimensions[0], 0, 0], \
            [dimensions[0], dimensions[1], 0], [0, dimensions[1], 0]])

        lowBase = lowBase.moved(position)


        (vertices, faces) = self.prismBasesToStructure(lowBase, dimensions[2])

        self.vertices = vertices
        self.faces = faces

        return







if __name__ == "__main__":
    import doctest
    doctest.testmod()