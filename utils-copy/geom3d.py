import numpy as np

from mainutils import *
from geomutils import *

class LineSegment3D(object):
    """ 3D Line Segment class. 

        >>> line = LineSegment3D(np.array([0, 1, 0]), np.array([1, 0, 0]))
        >>> line5 = LineSegment3D(np.array([1, 0, 0]), np.array([0, 1, 0]))
        >>> line == line5
        True
        >>> np.array_equal(line.move(), np.array([1, -1, 0]))
        True
        >>> line.length() == sqrt(2)
        True
        >>> line2 = LineSegment3D(np.array([0, 0, 0]), np.array([1, 1, 1]))
        >>> line2 == line
        False
        >>> line2.length() == sqrt(3)
        True
        >>> line3 = LineSegment3D(np.array([0.5, 0.5, 0]), np.array([2, -1, 0]))
        >>> line4 = LineSegment3D(np.array([0.5, 0.5, 0]), np.array([1, 0, 0]))
        >>> line.sameLineIntersection(line3) == line4
        True
    """
    
    #@signature(Any, np.array, np.array)
    def __init__(self, start, end):
        """
            :param start: start of line segment- Numpy array (3D vector)
            :param end: end of line segment - Numpy array (3D vector)
        """
        self.start = start
        self.end = end


    #__str__ or __repr__?
    def __repr__(self):
        return "LineSegment3D({0}, {1})".format(self.start, self.end)

    #print LineSegment(np.array([0, 1]), np.array([1, 0])).move()

    #TODO: accuracy

#   def containsPoint(self, point):
#       if self

    @log_with()
    def __eq__(self, line2):
        """ Line segments are equal if they are same geometry set (order of segments is not important).
        """
        return (isinstance(line2, self.__class__) and 
            (set([totuple(self.start), totuple(self.end)]) == set([totuple(line2.start), totuple(line2.end)])))

    def __hash__(self):
        return hash(frozenset([totuple(self.start), totuple(self.end)]))

    #FIXME: hash

    def __ne__(self, other):
        return not self.__eq__(other)

    def __call__(self, x):
        return self.start + self.move()*x/self.length()

    def move(self):
        return self.end - self.start

    def length(self):
        return np.linalg.norm(self.move())

    def plot(self, plotter=None, show=False):
        print "plot"
        kwargs = {}

        plotCrd = np.array([self.start, self.end]).T

        if plotter==None:
            #scatter3d(*plotCrd)
            plot3d(*plotCrd)
        else:
            plotter.plot3d(*plotCrd, elementType="line", **kwargs)

        if show==True: plt.show()

    @log_with()
    def sameLineIntersection(self, line):
        """ Calcucalate intersection of two line segments.
            These line segments must lie on the same line (with some epsilon; this is not checked).
            Points are treated as they would lie on same line (with some epsilon).

            :param line: second line
            :returns: intersection geometry
        """
            #we can't sort line points without knowing self order?
        pos1 = pointLinePos(self, line.start)
        pos2 = pointLinePos(self, line.end)



        #posSet = set(pos1, pos2)
        #TODO: rename inter (intersection or some good shortcut)?

        if pos1>pos2:
            #swap points; start should be before end
            pos1, pos2 = pos2, pos1 #swap Points
            line.start, line.end = line.end, line.start #swap 


        if pos1==pos2==2 or pos1==pos2==-2:
            #--x--x--*--*---
            #line segments not intersects
            result = emptyGeometry #TODO:empty geometry set

        elif (pos1, pos2)==(2,-2): #((2, -2), (-2, 2)):
            #--*--x--x--*---
            result = LineSegment3D(self.start, self.end)

        elif (pos1, pos2)==(0, 0):
            #--x--*--*--x---
            result = LineSegment3D(line.start, line.end)

        elif (pos1, pos2)==(-2, 0):
            #--2--1--2--1---
            result = LineSegment3D(self.start, line.end)

        else:
            #--1--2--1--2---
            #(pos1, pos2) == (0, 2)
            result = LineSegment3D(line.start, self.end) #line.end


        return result

    #def crosses(self, segment2):



#maybe method of LineSegment3d?
@log_with()
def pointLinePos(line, point):
    """ Return position point to line segment.
        Line orientation is from start to end.
        Point must be part of line containg line segment.
        :returns: -2 - point before start
            -1 - point on start
            0 - point between start and end
            1 - point on end
            2 - point after end
    """
    log.info((line, point))
    #one line - norm is changing vector to "real"(float) number
    vec1 = norm(line.end - line.start)
    vec2 = norm(point - line.start)
    vec3 = norm(point - line.end)

    if np.allclose(vec2+vec1, vec3):
        #Revert vector for comparing according to line direction
        vec2 = -vec2

    #TODO: compare to point (with epsilon?)
    if vec2>vec1:
        return 2

    elif vec2>0:
        return 0

    else:
        return -2


class RangeHelper(object):
    @staticmethod
    def resizeRange(range, factor):
        #Approximation comment: this method may introduce round-off error.
        reduce = (range.end - range.start) * (1 - factor) / 2
        return Range(range.start+reduce, range.end-reduce)


class Range(object):
    """ N-dimensional range class.
        Range is closed on minimal half of faces and closed on maximal half of faces.
        Minimal vertex of range boundary is in range. Other vertices are outside range.
    """
    #How should I represent 2d range in 3d space?
    #TODO - class not ended

    #TODO: write in Cython - intersects of two rangeArrays
    #Or better - intersects of all zipped array pairs from two big arrays
    #@contract(rangeArray='array[2xN]')
    def __init__(self, rangeArray):
        """ Init range.
        :param rangeArray: two rows - start point and end point
        :return:
        """
        # if isinstance(self, Range):
        #     range = rangeArray
        #     self.rangeArray = range.rangeArray
        self.rangeArray = rangeArray

    #def create(self, start, end):
    @staticmethod
    def create(center=None, radius=None):
        #Create Range from different parameters
        #radius - half of range edge?
        if center!=None and radius!=None:
            npCenter = np.array(center)
            return Range(np.array([npCenter - [radius]*len(center), npCenter + [radius]*len(center)]))

    @property
    def start(self):
        return self.rangeArray[0]

    @property
    def end(self):
        return self.rangeArray[1]

    def insideRange(self, point):
        """ Is point inside range3d. """
        #Approximation comment: each point will be contained in some range of grid.
        startb1 = Range.greaterOrEqual(point, self.start)
        startb2 = Range.smallerPoint(point, self.end)
        return (startb1 and startb2)


    def intersects(self, range2):
        """ Return true if two ranges (close-opened as stated in class definition) intersect.
        Intersection is calculated without approximation.
        :param range2: Range3d
        """
        #Approximation comment: This method should not introduce approximation error.
        #Approximated range may return wrong results when boundaries of ranges are near.

        #Is this method readable or need refactoring?
        if not Range.greaterPoint(range2.end, self.start):
            return False
        elif not Range.smallerPoint(range2.start, self.end):
            return False
        else:
            return True


    # def project(self, dimension):
    #     #project ortogonally
    #     #We project to new space or only we vanish one dimension?
    #     #maybe project on one dimension?
    #     #TODO
    #     pass



    def realDimension(self):
        #Real dimension of range "volume" set.
        #How to implement this?
        #What if range is degenerated?

        #Approximation comment: App. range may return wrong result when some dimension is very small.
        #TODO: check if this work
        raise NotImplementedError()
        #dimensionCount = (self.rangeArray[0]!=self.rangeArray[1]).count
        #return dimensionCount

    @property
    def dimension(self):
        """ Dimension of space in which range is embedded. """
        #Number of columns
        return self.rangeArray[0].shape[1]


    # def plot(self, plotter):
    #     #TODO: plotType?
    #     if self.realDimension()==1:
    #         plotter.plot(self.rangeArray.T, plotType="lines")
    #
    #     #elif self.realDimension()==2:
    #     elif (self.realDimension() in [2, 3]):
    #         for n in range(0, self.dimension):
    #             #TODO: zlozonosc silni wzgledem ilosci wymiarow
    #             range = self.rangeArray
    #             if range[1,n]==range[0,n]:
    #                 #Degenerated dimension
    #                 continue
    #
    #             #Max of this dimension
    #             range[1,n] = range[0,n]
    #             range = Range(range)
    #             range.plot(plotter)
    #
    #             #Min of this dimension
    #             range[0,n] = range[1,n]
    #             range = Range(range)
    #             range.plot(plotter)
    #
    #             #That should work
    #
    #         pass
    #     else:
    #         raise Exception("Wrong dimension for plotting")

    #Maybe move these two methods to module?
    @staticmethod
    #@contract(p1='array[N]', p2='array[N]')
    def greaterPoint(p1, p2):
        """ Return true if p1>p2, where ">" relation is partial order
                defined as - all coordinates of p1 are greater than of p2.
        :param p1: point 3d
        :param p2: point 3d
        """
        return all(p1 > p2)

    def greaterOrEqual(p1, p2):
        #This function should not have problem with approximate computation, if correctly used.
        return all(p1 >= p2)

    @staticmethod
    #@contract(p1='array[N]', p2='array[N]') - contracts use 75% time of computation
    def smallerPoint(p1, p2):
        """ p1 < p2"""
        return Range3d.greaterPoint(p2, p1)

Range3d = Range


if __name__ == "__main__":
    import doctest
    doctest.testmod()