PolyhedronsCG - project for computing intersection of two convex polyhedrons
===============================================================

![Visualization](https://bitbucket.org/wojtek132/polyhedronscg/raw/6e7a1e399d75febd8dd75ac257254e79ff9b7f58/vis/mayavi-tetrahedronsWithIntersectionLine.png)

Program will compute line of intersection of boundaries of two convex polyhedrons. Program will use algorithm that check all pairs of polygons from these two polyhedrons.

Program can additionally:

- load polyhedrons from ply files 
- save polyhedrons to ply files
- plot polyhedrons and its intersections
	
See documentation (only in polish) for details.

Command for running program:
```
python polyhedron.py
```

Example usage:
```
	listPolyhedrons
	loadPolyhedron cuboid.ply
	loadPolyhedron cuboid2.ply
	listLoadedPolyhedrons
	intersection 3 4
	plot
```
