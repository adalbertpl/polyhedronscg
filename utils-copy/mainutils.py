# -*- coding: utf-8 -*-
from __future__ import division

#Source code of autoassign from:
#http://code.activestate.com/recipes/551763-automatic-attribute-assignment/

import sys
import pdb
import traceback
#import os

import random

try:
    import numpy as np
except ImportError:
    print "Error: no numpy" #For example pypy may not contain numpy

from math import *
#TODO: uzywanie from w modulach chyba spowoduje, 
#ze te nazwy beda takze widoczne w innych plikach w ktorych jest from

from functools import wraps, update_wrapper
from inspect import getargspec, isfunction
from itertools import izip, ifilter, starmap, tee
import itertools
import inspect




import logging

#TODO: default logger to file
#TODO: When these lines are set, settings from modules not work
#It should depend which config is last set beofre creating logger?
# logfile = open("other.log", "w+")

# logging.basicConfig(stream=logfile, level=logging.DEBUG)
# log = logging.getLogger('geomutils')
# log.setLevel(logging.DEBUG)

#fh = logging.FileHandler('other.log')
#log.addHandler(fh)



#TODO: equality, not equality, hash - auto by some values

def autoassign(*names, **kwargs):
    """
    autoassign(function) -> method
    autoassign(*argnames) -> decorator
    autoassign(exclude=argnames) -> decorator
    
    allow a method to assign (some of) its arguments as attributes of
    'self' automatically.  E.g.
    
    >>> class Foo(object):
    ...     @autoassign
    ...     def __init__(self, foo, bar): pass
    ... 
    >>> breakfast = Foo('spam', 'eggs')
    >>> breakfast.foo, breakfast.bar
    ('spam', 'eggs')
    
    To restrict autoassignment to 'bar' and 'baz', write:
    
        @autoassign('bar', 'baz')
        def method(self, foo, bar, baz): ...

    To prevent 'foo' and 'baz' from being autoassigned, use:

        @autoassign(exclude=('foo', 'baz'))
        def method(self, foo, bar, baz): ...
    """
    if kwargs:
        exclude, f = set(kwargs['exclude']), None
        sieve = lambda l:ifilter(lambda nv: nv[0] not in exclude, l)
    elif len(names) == 1 and isfunction(names[0]):
        f = names[0]
        sieve = lambda l:l
    else:
        names, f = set(names), None
        sieve = lambda l: ifilter(lambda nv: nv[0] in names, l)
    def decorator(f):
        fargnames, _, _, fdefaults = getargspec(f)
        # Remove self from fargnames and make sure fdefault is a tuple
        fargnames, fdefaults = fargnames[1:], fdefaults or ()
        defaults = list(sieve(izip(reversed(fargnames), reversed(fdefaults))))
        @wraps(f)
        def decorated(self, *args, **kwargs):
            assigned = dict(sieve(izip(fargnames, args)))
            assigned.update(sieve(kwargs.iteritems()))
            for _ in starmap(assigned.setdefault, defaults): pass
            self.__dict__.update(assigned)
            return f(self, *args, **kwargs)
        return decorated
    return f and decorator(f) or decorator
    
    

import collections
import functools


def initFileLogger(name):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    fh = logging.FileHandler(name+'.log')
    # %(module)s:
    fm = logging.Formatter('[%(created)f] %(name)s:%(funcName)s:%(lineno)d  %(message)s')
    fh.setFormatter(fm)
    log.addHandler(fh)

    return log

#TODO: check if this works for methods 
class memoized(object):
    '''Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated).
    '''
    #From: https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    def __init__(self, func):
       self.func = func
       self.cache = {}
    def __call__(self, *args):
       if not isinstance(args, collections.Hashable):
          # uncacheable. a list, for instance.
          # better to not cache than blow up.
          return self.func(*args)
       if args in self.cache:
          return self.cache[args]
       else:
          value = self.func(*args)
          self.cache[args] = value
          return value
    def __repr__(self):
       '''Return the function's docstring.'''
       return self.func.__doc__
    def __get__(self, obj, objtype):
       '''Support instance methods.'''
       return functools.partial(self.__call__, obj)



class Lazy(object):
    """ Lazy property decorator.
    """
    #From: http://code.activestate.com/recipes/363602/
    def __init__(self, calculate_function):
        self._calculate = calculate_function

    def __get__(self, obj, _=None):
        if obj is None:
            return self
        value = self._calculate(obj)
        setattr(obj, self._calculate.func_name, value)
        return value

#@Lazy
#o.someprop
#del o.someprop - will be recalculated next time?


def addToClass(cls):
    #not my code; code from Compilation Theory classes
    def decorator(func):
        setattr(cls,func.__name__,func)
        return func
    return decorator



#https://wiki.python.org/moin/PythonDecoratorLibrary#Logging_decorator_with_specified_logger_.28or_default.29

import functools, logging, logging.handlers

#TODO: problems with log_with, when added handler in module, when decorator is used

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
log.addHandler(logging.FileHandler('mainutils.log'))

#handler = logging.handlers.RotatingFileHandler(
#              "geomutils.log", maxBytes=10000, backupCount=5)

#log.addHandler(handler)

#TODO: log_all_methods - for class
#now @log_with() SomeClass - is useless, i think
#TODO: log function parameters

class log_with(object):
    '''Logging decorator that allows you to log with a
specific logger.
'''
    #TODO: maybe append class name
    #FIXME: seems to not showing exception when attempt to show log
    # Customize these messages
    #ENTRY_MESSAGE = 'Entering {}'
    ENTRY_MESSAGE = 'Entering {} with args: {}, {}'
    EXIT_MESSAGE = 'Exiting {}' #- version 1.0
    #TODO: why is logging not showing in version 1.1?
    #EXIT_MESSAGE = 'Exiting {0}, result={1}'

    def __init__(self, logger=None, details=2):
        """
        :param logger: Logger
        :param details: level of details of logger;
            better details - may take more time and more place in log
        :return: decorator funtion
        """
        self.logger = logger

    def __call__(self, func):
        '''Returns a wrapper that wraps func.
The wrapper will log the entry and exit points of the function
with logging.INFO level.
'''
        # set logger if it was not set earlier
        if not self.logger:
            #logging.basicConfig() - with basic config, configuration from other module seems not working?
            #   for some modules it works, for some not?
            #Should default logger log to stdout or to some default file?
            #Where should be this default file?
            self.logger = logging.getLogger(func.__module__)

        @functools.wraps(func)
        def wrapper(*args, **kwds):
            try:
                #self.logger.info(self.ENTRY_MESSAGE.format(func.__name__)
                strArgs = [str(arg)[0:40] for arg in args] #+ "..."
                strKwargs = [str(kw)[0:40] for kw in kwds]

                self.logger.info(self.ENTRY_MESSAGE.format(func.__name__, strArgs, strKwargs))  # logging level .info(). Set to .debug() if you want to
                f_result = func(*args, **kwds)
                self.logger.info(self.EXIT_MESSAGE.format(func.__name__)) #- version 1.0
                #self.logger.info(self.EXIT_MESSAGE.format(func.__name__, f_result))
                return f_result
            except Exception as e:
                #Use, when logging has errors (e.g. with function results or arguments)
                #and when we cannot print to stdout.
                self.logger.info("Error when logging: "+str(e.message))
        return wrapper

#Create default logger for decorator
logger2 = logging.getLogger('default')
logger2.setLevel(logging.DEBUG)

fh = logging.FileHandler('default.log')
logger2.addHandler(fh)

log_dec = log_with(logger2)

def newLoggingDec(logger):
    """ Create decorator for logging methods.
        :param logger: logger or name of logger
        :returns: decorator around (possibly created) logger
    """
    if isinstance(logger, str):
        loggerName = logger
        logger = logging.getLogger(loggerName)
        logger.setLevel(logging.DEBUG)

        fh = logging.FileHandler('{}.log'.format(loggerName))
        logger.addHandler(fh)

    return log_with(logger)



def runtimedebug(fn):
    """ Function decorator.
        Decorate function fn as debug-mode funtion, which will be checked on run-time
            (every time function is run).
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        global debugMode
        #TODO: not work - how to make debugMode value global for many modules?
        #Maybe debugMode function in mainutils?
        if debugMode == True:
            return fn(*args, **kwargs)
        else:
            return

    return wrapper    

#When to check debugmode? When decorating or when running?

def debugprint(level="long"):
    """ Decorator for printing function arguments.
        :param level: How verbose print
    """

    #What is scope of before and after functions?
    if level=="long":
        def before(fun, args, kwargs): 
            print "Run: {}, with args: {}, kwargs: {}".format(str(fun.__name__), str(args), str(kwargs))
        def after(fun, f_result):
            print "End: {}, result: {}".format(str(fun.__name__), str(f_result))

    elif level=="short":
        def before(fun, args, kwargs):
            print "{}({}, {})...".format(fun.__name__, str(args), str(kwargs))
        def after(fun, f_result):
            print "\t{}".format(str(f_result))

    elif level=="result":
        def before(fun, args, kwargs): return

        def after(fun, f_result):
            print "{}".format(str(f_result))   

    #before = runtimedebug(before)
    #after = runtimedebug(after)

    #It seems to work
    def decorate(fun):

        @wraps(fun)
        def wrapper(*args, **kwargs):
            before(fun, args, kwargs)
            f_result = fun(*args, **kwargs)
            #FIXME: why some function run before and not run after?
            after(fun, f_result)

            return f_result

        return wrapper

    return decorate


# import traceback
#
# def catchExceptions(exceptions=None, message="stack"):
#     #TODO: message type
#     #TODO: not work (with cmd module?)
#
#     if exceptions==None:
#         exceptions = [Exception]
#
#     def decorate(fn):
#         from functools import wraps
#         @wraps
#         def wrapped(*args, **kwargs):
#             try:
#                 return fn(*args, **kwargs)
#             except exceptions as e:
#                 print(e)
#                 print(str(traceback.format_exc()))
#
#
#         return wrapped
#
#     return decorate


def Any(someObject):
    """ Every type of object.
        Needed for signature function
        :author: Wojtek
    """
    return someObject

def callable(obj):
    """ Check if function is callable (i.e. is a function or a functor). 
        :returns: True if function is callable.
    """
    return hasattr(obj, '__call__')

def signature(*args, **kwargs):
    #TODO: is there a way to use only one def, not three?
    #FIXME: this decorator seems to break doctest
    #TODO: maybe @wraps?
    #Ten dekorator chyba nie przyda sie do rzutowania typow zlozonych np. listy obiektow
    def decorator(fn):
        def wrapped(*fn_args, **fn_kwargs):
            new_args = []
            for t, raw in zip(args, fn_args):
                #troche tu zmienilem - Wojtek
                #if t = object
                new_args.append(t(raw))

            new_kwargs = dict([(k, kwargs[k](v)) for k, v in fn_kwargs.items()])

            fn(*new_args, **new_kwargs)

        return wrapped

    return decorator

def typecast(*args, **kwargs):
    """ Decorator for casting to another type if type is different.
    """
    castings = {}
    #TODO: przetestowac dekorator, bo go jeszcze nie sprawdzalem

    #Wzorowalem sie na dekoratorze signature
    def decorator(fn):
        @wraps
        def wrapper(*fn_args, **fn_kwargs):
            new_args = []
            for argType, arg in zip(args, fn_args):
                if isinstance(argType, str):
                    #TODO: parse
                    #Special types
                    #Collections etc.
                    pass

                elif isinstance(arg, argType):
                    new_args.append(arg)

                elif castings.hasKey((type(arg), argType)):
                    conversionFun = castings((type(arg), argType))
                    new_args.append(conversionFun(arg))
                    #TODO: not ended
                    pass
                else:
                    raise Exception("Not implemented conversion")


def classInvariant(predicate, action="log"):
    """ Check invariant of class instance after each method is run.

    :param predicate: function of object;
        function should return true if invariant is preserved, false otherwise
    :param action: Specify action, when invariant is not preserved
    :return: decorator
    """
    def decorate(cls):
        for_all_methods(after(predicate))(cls)

    return decorate


def after(action):
    """ Decorator for running function action after function fn.
    """

    def decorate(fn):
        #log.info("after - decorate - {}".format(fn))
        @wraps(fn)
        def wrapped(*args, **kwargs):
            #Only for testing decorator:
            # log.info("after - decorate - wrapped - {}, {}".format(args, kwargs))
            result = fn(*args, **kwargs)
            action(args[0]) #FIXME: change
            return result

        return wrapped

    return decorate


def for_all_methods(decorator, methods=None, exclude=[]):
    """ Class decorator. Decorate all methods of decorated class.
        Not use both methods and exclude.
        It may not be used as decorator: 
            for_all_methods(debugprint())(Crossover)

        :param decorator: will decorate all methods of class
        :param methods: names of methods to decorate (None - decorate all)
        :param exclude: names of methods to exclude
    """
    #TODO: "exclude methods" argument
    #TODO: private methods name are obfuscated
    #Based on: http://stackoverflow.com/questions/6307761/how-can-i-decorate-all-functions-of-a-class-without-typing-it-over-and-over-for
    #Test if update_wrapper works
    def decorate(cls):
        #print "test"
        #print inspect.getmembers(cls, predicate=inspect.ismethod)
        for method in inspect.getmembers(cls, predicate=inspect.ismethod):
            #TODO: only public methods option
            attr = method[0]
            #setattr(cls, attr, getattr(cls, attr))
            #m = getattr(cls, attr)
            #print ((m, inspect.getargspec(m)))
            #It seems to work
            #Maybe change that we could use "decorator" instead of "decorator()"
            if (methods==None or (attr in methods)) and (not (attr in exclude)):
                setattr(cls, attr, decorator(getattr(cls, attr)))
            #m = getattr(cls, attr)
            #print ((m, inspect.getargspec(m)))


        #setattr(cls, attr, update_wrapper(log_with, getattr(cls, attr)))
        #Sa jakies problemy przy powyzszej linii - AttributeError: attribute '__doc__' of 'type' objects is not writable
        #oraz po drobnej zmianie: AttributeError: 'FloatRastriginEvaluationVerbose' object has no attribute '__name__'

        #Not worked for inherited methods?:
        # for attr in cls.__dict__:
        #     print attr
        #     print getattr(cls, attr)
        #     if callable(attr):
        #         #setattr(cls, attr, update_wrapper(decorator, getattr(cls, attr)))
        #         #print ((cls, attr))
        #         setattr(cls, attr, getattr(cls, attr))

        return cls

    return decorate



def timeit(fun, *params):
    """ Measure time of function result computation.
        :returns: computation time in seconds
    """
    #Written by me
    #Maybe change to decorator?
    #Maybe two versions?

    start = time.time()

    result = fun(*params)

    end = time.time()

    return end-start



def debug_on(*exceptions):
    """ Decorator for pdb debugging when exception is thrown.
        Example usage:
            @debug_on()
            def some_method()
    """
    #http://stackoverflow.com/questions/4398967/python-unit-testing-automatically-running-the-debugger-when-a-test-fails
    #CC licence
    #Nie sprawdzalem, czy dziala. Ma dzialac z unittest. Z doctest cos nie chce dzialac.
    if not exceptions:
        exceptions = (AssertionError, )
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exceptions:
                info = sys.exc_info()
                traceback.print_exception(*info) 
                pdb.post_mortem(info[2])
        return wrapper
    return decorator



# --- functional programming elements ---

def pairwise(iterable):
    """ Not cycled (without (sn, s0).

        >>> list(pairwise([0, 1, 2]))
        [(0, 1), (1, 2)]
    """
    #Using: http://stackoverflow.com/questions/5764782/iterate-through-pairs-of-items-in-a-python-list
    #Creative Commons
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

def cyclePairwise(l):
    """ Return list elements pairwise, with cycle (sn, s0).
        @see pairwise
        
        :param l: list
        :returns: iterable?

        >>> list(cyclePairwise([0, 1, 2]))
        [(0, 1), (1, 2), (2, 0)]
    """
    #TODO: iterable
    return pairwise(l + [l[0]])


def parseFloats(s):
    """ Get float numbers from string.
        :param s: string to parse
        :returns: List of floats
        >>> parseFloats("0 0.5 1.0")
        [0.0, 0.5, 1.0]
    """
    #TODO: what with multiline
    return map(float, s.split())

# def grouper(n, iterable, fillvalue=None):
#     """ Group iterable to n-size tuples.
#         >>> grouper(3, 'ABCDEFG', 'x')
#         ABC DEF Gxx
#     """
#     args = [iter(iterable)] * n
#     #FIXME: zip_longest
#     return itertools.zip_longest(*args, fillvalue=fillvalue)

def groupElements(l, groupSize):
    return zip(*(iter(l),) * groupSize)


def initDefaultLogger(moduleName):
    logfile = open("{0}.log".format(moduleName), "w+")

    logging.basicConfig(stream=logfile, level=logging.DEBUG)
    log = logging.getLogger(moduleName)
    log.setLevel(logging.DEBUG)
    log.info('{0} module'.format(moduleName))


class Indexable(object):
    """ Sequence contain generator and can be indexed.
        Items are cached in memory.

        >>> gen = (i for i in xrange(20))
        >>> gen = Indexable(gen)
        >>> gen[5]
        5
        >>> gen[7:10]
        [7, 8, 9]
        >>> gen[0]
        0
    """
    #Class from: http://stackoverflow.com/questions/2322642/index-and-slice-a-generator-in-python?rq=1
    #There is another option, as stated in stackoverflow - run generator from start every time
    def __init__(self,it):
        self.it = iter(it)
        self.already_computed=[]
    def __iter__(self):
        for elt in self.it:
            self.already_computed.append(elt)
            yield elt
    def __getitem__(self,index):
        try:
            max_idx=index.stop
        except AttributeError:
            max_idx=index
        n=max_idx-len(self.already_computed)+1
        if n>0:
            self.already_computed.extend(itertools.islice(self.it,n))

        return self.already_computed[index]    



def listify(fn=None, wrapper=list):
    """
    A decorator which wraps a function's return value in ``list(...)``.

    Useful when an algorithm can be expressed more cleanly as a generator but
    the function should return an list.

    Example::

        >>> @listify
        ... def get_lengths(iterable):
        ...     for i in iterable:
        ...         yield len(i)
        >>> get_lengths(["spam", "eggs"])
        [4, 4]
        >>>
        >>> @listify(wrapper=tuple)
        ... def get_lengths_tuple(iterable):
        ...     for i in iterable:
        ...         yield len(i)
        >>> get_lengths_tuple(["foo", "bar"])
        (3, 3)
    """
    #From: http://stackoverflow.com/questions/12377013/is-there-a-library-function-in-python-to-turn-a-generator-function-into-a-functi
    def listify_return(fn):
        @wraps(fn)
        def listify_helper(*args, **kw):
            return wrapper(fn(*args, **kw))
        return listify_helper
    if fn is None:
        return listify_return
    return listify_return(fn)


def namedtuple_with_defaults(typename, field_names, default_values=()):
    #http://stackoverflow.com/questions/11351032/named-tuple-and-optional-keyword-arguments
    #Stackoverflow: One problem with the wrapper version: unlike the builtin collections.namedtuple, this version is not pickleable/multiprocess serializable if the def() is included in a different module.
    T = collections.namedtuple(typename, field_names)
    T.__new__.__defaults__ = (None,) * len(T._fields)
    if isinstance(default_values, collections.Mapping):
        prototype = T(**default_values)
    else:
        prototype = T(*default_values)
    T.__new__.__defaults__ = tuple(prototype)
    return T


#TODO: class for converting between types in decorator.


def all_same(items):
    return (len(set(items)) == 1)

def _all_same2(items):
    return all(x == items[0] for x in items)





# def projectOnPlane(point, plane):
#   """ Project 3D point onto plane.
#       :param point: np.array
#       :param plane: ()
#   """


if __name__ == "__main__":
    import doctest
    doctest.testmod()