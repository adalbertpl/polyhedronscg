
import sys
import os

sys.path.append(os.path.expanduser('./utils-copy'))
#sys.path.append(os.path.expanduser('~/AGH/Python/utils'))

import logging
import random
import traceback


logfile = open("geomutils.log", "w+")

logging.basicConfig(stream=logfile, level=logging.DEBUG)
log = logging.getLogger('geomutils')
log.setLevel(logging.DEBUG)
log.info('polyhedron module')


#import scipy.spatial
import numpy as np
try:
    import matplotlib.pyplot as plt
except:
    log.warning("No matplotlib. We could not use matplotlib plots.")
    #print "No matplotlib. We could not use matplotlib plots."

from math import *
import cmd

#import timeit
import time

from mainutils import *
from geomutils import *
from geom3d import *

from prism import Cuboid, Polygon3d, Polyhedron










def polyhedronIntersects(ph1, ph2):
    """ Return if two polyhedrons intersects.
        Function is numerical, not symbolical.
        :param ph1: first polyhedron
        :param ph2: second polyhedron
        :returns: true if intersection is not empty, false otherwise.

    >>> cuboid1 = CuboidFactory.fromTwoVertices([0,0,0], [2,2,1])
    >>> cuboid2 = CuboidFactory.fromTwoVertices([2.5,2.5,0], [4,4,1])
    >>> cuboid3 = CuboidFactory.fromTwoVertices([1,1,0.5], [3,3,1.5])
    >>> polyhedronIntersects(cuboid1, cuboid2)
    False
    >>> polyhedronIntersects(cuboid1, cuboid3)
    True
    >>> polyhedronIntersects(cuboid2, cuboid3)
    True
    """

    def debug():
        pol1.plotMayavi(color=(0,1,0))
        [pol2.plotMayavi() for pol2 in ph2.getPolygons()]

        mlab.axes()
        mlab.show()


    for pol1 in ph1.getPolygons():
        #debug()

        for pol2 in ph2.getPolygons():
            if pol1.doIntersects(pol2):
                return True

    #No pair of polygons intersects
    return False


#Geomutils contains class Cuboid too
class CuboidFactory:
    @staticmethod
    def fromTwoVertices(start, end):
        """ Create cuboid from two vertices connected by diagonal.
            :param start: start point (should be nd.array)
            :param end: end point (should be nd.array)
            :returns: Cuboid object
        >>> cuboid1 = CuboidFactory.fromTwoVertices([1,0.5,0.5], [2,2,1])
        >>> cuboid2 = CuboidFactory.fromTwoVertices([1,0.5,1], [2,2,1])
        >>> (cuboid1, cuboid2)
        >>> cuboid1 == cuboid2
        False
        >>> cuboidPolygons = cuboid1.getPolygons()
        >>> polygon1 = Polygon3d([[1,0.5,0.5], [1,0.5,1], [1,2,1], [1,2,0.5]])
        >>> polygon1 in cuboidPolygons
        True
        """
        #Format conversion
        start, end = toNumpy(start), toNumpy(end)

        return Cuboid(end-start, position=start)

#class PolyhedronFactory

#Python has duck typing
#Maybe class hierarchy of geometry is not needed?







#W algorytmie Mullera-Preparaty sciany maja zawierac chyba rownanie plaszczyzny






#TODO: create line by moving along axis from last point of line?

# def polyhedronsIntersection(ph1, ph2):
#     #Input for polygon should not contain last vertex (repeated first vertex)
#     return Polygon3d(polyhedronsIntersectionLine(ph1, ph2)[0:-1])

def polyhedronsIntersectionLine(ph1, ph2):
    """ Compute intersection chain (of line segments) of two polyhedrons.
        This is geometry representing intersection of boundaries of ph1 and ph2.
        This chain should be closed (from definition).
        E.g. for two cubes it will be chain of six points.
        :param ph1: polyhedron1
        :param ph2: polyhedron2
        :returns: list of vertices of intersection chain;
            last vertex should be treated as connected with first

    >>> cuboid1 = CuboidFactory.fromTwoVertices([0,0,0], [2,2,1])
    >>> cuboid3 = CuboidFactory.fromTwoVertices([1,1,0.5], [3,3,1.5])
    >>> chain = polyhedronsIntersectionLine(cuboid1, cuboid3)
    >>> set(chain) == set([[1,2,0.5], [1,2,1], [1,1,1], [2,1,1], [2,1,0.5],[2,2,0.5]])
    True
    """
    #Works only for conves polyhedrons
    lines = []

    #plotter = MatplotlibPlotter(show=False)

    #TODO: numerical accuracy
    #TODO: refactor
    for pol1 in ph1.getPolygons():
        for pol2 in ph2.getPolygons():
            polIntersection = pol1.intersection(pol2)
            #2 1 0.5, 2 1 1 - tu jest jakis problem moze?

            #plt.show(block=False)
            #plotter.clf()
            #pol1.plot(plotter=plotter)
            #pol2.plot(plotter=plotter)
            #plt.draw()


            if not isinstance(polIntersection, (np.ndarray, LineSegment3D, EmptyGeometry)):
                raise Exception("Not possible geometry result: {0}".format(polIntersection))

            if (polIntersection != []) and (polIntersection != emptyGeometry):
                #polygons intersects
                log.info("Intersection of polygons: {0}".format(polIntersection))
                #polIntersection.plot(plotter=plotter)
                lines.append(polIntersection)

    print "Lines: {}".format(lines)

    if len(lines)==0:
        return EmptyGeometry()

    else:
        polygonVertices = []

        chainParts = []

        def chainPartsEnds():
            #return np.array([part[-1] for part in chainParts])
            return [part[-1] for part in chainParts]

        def chainPartsStarts():
            #return np.array([part[0] for part in chainParts])
            return [part[0] for part in chainParts]

        #chainPartsEnds = []

        [chainParts.append([totuple(line.start), totuple(line.end)]) for line in lines]

        from collections import Counter

        #[chainPartsEnds.append(line.end) for line in lines]

        from itertools import imap

        # class npList(list):
        #     def __contains__(self, item):
        #         #TODO: lazy map and filter
        #         return any(imap(lambda x: np.allclose(x, item), item))

            #def __getitem__(self, item):

        def endCondition(chainParts):
            #not len(chainParts)>1
            #It seems to work
            return all([doClosedChain(part) for part in chainParts])

        def doClosedChain(chain):
            return chain[0]==chain[-1]

        while not endCondition(chainParts):
            #pdb.set_trace()
            #link all parts in one chain
            notClosed = filter(lambda x: not doClosedChain(x), chainParts)
            chainParts.remove(notClosed[0])
            part = notClosed[0]
            log.info("ChainParts: {0}".format(chainParts))
            log.info("ChainPartsEnds: {}. part: {}".format(chainPartsEnds(), part))

            c = Counter(asListOfTuples(chainPartsEnds())) + Counter(asListOfTuples(chainPartsStarts()))
            print c.values(), c
            #assert all(map(lambda x: x==2,c.values())), "parts not form cycle:\n {0}".format(chainParts)

            # @property
            # def chainPartsNp():
            #     return np.array(chainParts):

            def doNpArrayInList(array, l):
                for el in l:
                    if np.allclose(array, el):
                        return True

                return False

            def getNpArrayFromList(predicate, l):
                """ Get first element matching predicate """
                for el in l:
                    if predicate(el):
                        return el

                return None

            #chainPartsNp = np.array(chainParts)

            #We have two option - other chain (as exists) has to be reversed or not
            #if part[0] not in chainPartsEnds():
            #indices = np.all(chainPartsEnds()==part[0], axis=1)
            #if (chainPartsNp[:, indices]).size==0:
            if not doNpArrayInList(part[0], chainPartsEnds()):
                log.info("reverse chain")
                #part2 = chainPartsNp[:,(chainPartsStarts()==part[0]).all(axis=1)]
                part2 = filter(lambda x: np.allclose(x[0], part[0]), chainParts)[0]
                #log.info((str(part2), type(part2)))
                #log.info((str(chainParts), type(chainParts[0])))
                chainParts.remove(part2)
                log.info(list(reversed(part2)))
                chainParts.append(list(reversed(part2)))
                log.info("ChainParts in reverse: {}".format(chainParts))

            #chainPartsNp = np.array(chainParts)
            #indices = np.all(chainPartsEnds()==part[0], axis=1)
            #if (chainPartsNp[:, indices]).size==0:
            if doNpArrayInList(part[0], chainPartsEnds()):
                log.info("add chain 'part' to end of another chain")
                #Concatenate two chains
                #index = findRowIndex(chainPartsEnds(), part[0]) #should be exactly one index

                other = filter(lambda x: np.allclose(x[-1], part[0]), chainParts)[0]
                log.info("other: {}".format(other))

                # assert len(index)==1, "wrong number of indices (should be 1), index={0}".format(index)
                # log.info(index)

                #other = chainParts[index]
                other.extend(part[1:])
                log.info("other after extend: {}".format(other))
                #FIXME: will it work?
                #Change end of one chain part in index
                #chainPartsEnds[chainPartsEnds==line.start] = line.end

            else:
                assert "Part[0] must be in chainPartsEnds()"
                log.info("add another chain (reversed) to this reversed chain 'part'")

                #chainParts.append(part2.reversed.extend(part))
                #chainParts.append(part.reversed.extend(reversed(part)))

                #chainParts.append([line.start, line.end])

                #chainPartsEnds.append(line.end)


        chain = chainParts[0]
        print ("Returned chain: {0}".format(chain))

        #Now all line segments are part of one chain
        #We can return this chain

        return Polyline(chain)




# def polyhedronsIntersection(ph1, ph2):
#     """ Compute intersection of two polyhedrons.
#         :returns: polyhedron - intersection

#     >>> cuboid1 = CuboidFactory.fromTwoVertices([0,0,0], [2,2,1])
#     >>> cuboid2 = CuboidFactory.fromTwoVertices([2,2,1], [0,0,0])
#     >>> intersection1 = polyhedronsIntersection(cuboid1, cuboid2)
#     >>> cuboid1.equals(intersection1)
#     True
#     >>> cuboid3 = CuboidFactory.fromTwoVertices([1,1,0], [3,3,1])
#     >>> cuboid4 = CuboidFactory.fromTwoVertices([1,1,0], [2,2,1])
#     >>> intersection2 = polyhedronsIntersection(cuboid1, cuboid3)
#     >>> cuboid4.equals(intersection2)
#     True
#     >>> cuboid4.equals(intersection1)
#     False
#     """
#     pass


def testPlot():
    cuboids = testData()
    # map(lambda x: x.plot(), cuboids)
    # plt.show()

    map(lambda x: x.plotMayavi(), cuboids)
    mlab.axes(extent=[0, 5, 0, 5, 0, 5])
    mlab.show()

def testData():
    cuboids = []
    cuboids.append(CuboidFactory.fromTwoVertices([0,0,0], [2,2,1]))
    cuboids.append(CuboidFactory.fromTwoVertices([2.5,2.5,0], [4,4,1]))
    cuboids.append(CuboidFactory.fromTwoVertices([1,1,0.5], [3,3,1.5]))

    return cuboids

class Color:
    _COLORS = ['b', 'r', 'g', 'y', 'm']

    @staticmethod
    def cycle(count):
        firstColors = Color._COLORS[0:count]
        return cycle(firstColors)

class PolyhedronCmd(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)

        self.polyhedrons = {}
        self.polygon = None
        self.count = 1

    def do_drawPolyhedron(self, line):
        print "Not implemented"
        #What this method should do?
        # try:
        #     self.crossings = None
        #     self.lines = LineCreator.create()
        #
        # except Exception as e:
        #     print(e)

    #FIXME: cos nie dziala z cmd w Sublime Text, zawsze sie zawiesza
    def do_test(self, line):
        print "test"

    def do_plot(self, line):
        try:
            #FIXME: sometimes error:
            #Process finished with exit code 139
            #Purpose of error may be in Mayavi.
            plotter = MayaviPlotter()
            #plotter = MatplotlibPlotter()
            #plt.show(block=False)
            for ph in self.polyhedrons.values():
                #colorFun = Color.cycle(5)
                ph.plot(show=False, plotter=plotter) #, color=colorFun())

            if self.polygon!=None:
                self.polygon.plot(show=False, plotter=plotter, color='r')
            #plotter.show() #Po plt.show() mozna obracac wykres, po draw nie.
            plotter.show()
        except Exception as e:
            print(e)
            print(traceback.format_exc())

    #reload modules after edit?

    def do_reflect(self, line):
        #It seems to work
        name = line.split()[0]
        ph = self.polyhedrons[name]
        ph.reflect()

    def do_move(self, line):
        try:
            #FIXME: not works
            #Usage: move polyhedronName 1 1 1
            name = line.split()[0]
            vector = line.split()[1:4]
            print vector
            vector = np.array(map(float, vector))
            ph = self.polyhedrons[name]
            ph2 = ph.moved(vector)
            self.addPolyhedron(ph2)
            print "Added new moved polyhedron: {}".format(ph2)
        except Exception as e:
            print(e)
            print(traceback.format_exc())

    def do_savePolyhedron(self, line):
        #arguments - [polyhedron name] [filename]
        #If you want to save as *.ply - file must contain extension.
        #If you want to save as *.dat - file may not contain extension
        try:
            phName, filename = line.split()
            ph = self.polyhedrons[phName]
            path = self.getPath(filename)
            ph.toPlyFile(path)
        except Exception as e:
            print(e)
            print(str(traceback.format_exc()))

    def getPath(self, filename):
        if "." not in filename:
            filename += ".dat"
        path = "polyhedrons/" + filename
        return path

    def do_loadPolyhedron(self, line):
        try:
            phName = str(self.count)
            self.count += 1
            path = self.getPath(line)
            ph = Polyhedron.fromPlyFile(path)
            self.polyhedrons[phName] = ph

        except Exception as e:
            print(e)
            print(traceback.format_exc())

    def addPolyhedron(self, ph):
        phName = str(self.count)
        self.count += 1
        self.polyhedrons[phName] = ph

    def do_listPolyhedrons(self, line):
        try:
            print os.listdir("polyhedrons")
        except Exception as e:
            print(e)

    def do_listLoadedPolyhedrons(self, line):
        try:
            for item in self.polyhedrons.items():
                print item
        except Exception as e:
            print(e)

    def do_intersection(self, line):

        try:
            ph1name, ph2name = line.split()
            #phIntersectionName = str(self.count)
            #self.count += 1

            ph1 = self.polyhedrons[ph1name]
            ph2 = self.polyhedrons[ph2name]
            #phIntersection = ph1.intersection(ph2)
            self.polygon = polyhedronsIntersectionLine(ph1, ph2)
            print "Intersection polygon:\n\t{}".format(self.polygon)
        except Exception as e:
            print(e)
            print(str(traceback.format_exc()))

    def do_removeLoaded(self, line):
            self.polyhedrons.pop(line)

    def do_intersectionCalcSim(self, line):
        try:
            print "Not implemented"
        except Exception as e:
            #TODO: change exception printing
            print(e)
            print(str(traceback.format_exc()))

    def do_doIntersects(self, line):
        try:
            ph1name, ph2name = line.split()
            ph1 = self.polyhedrons[ph1name]
            ph2 = self.polyhedrons[ph2name]

            result = polyhedronIntersects(ph1, ph2)
            if result==True:
                print "Polyhedrons intersects"
            else:
                print "Polyhedrons don't intersects"
        except Exception as e:
            print(e)

    #def do_loadExamples(self):


if __name__ == "__main__":
    #testPlot()

    cuboids = testData()
    #polyhedronIntersects(cuboids[0], cuboids[1])
    #polyhedronIntersects(cuboids[0], cuboids[2])
    #polyhedronsIntersectionLine(cuboids[0], cuboids[2])

    cmd1 = PolyhedronCmd()
    cmd1.do_loadPolyhedron("tetrahedron.ply")
    cmd1.do_loadPolyhedron("tetra2.ply")
    #main program loop
    cmd1.cmdloop()

    #import doctest
    #doctest.testmod()