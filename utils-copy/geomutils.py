from __future__ import division

import numpy as np

import base64
import json


#TODO: is sympy needed? new version is not in pip
#TODO: sympyutils (slow import ot sympy module)?

try:
    import sympy
except:
    #log.warning("Can't import sympy. But probably sympy is not used.")
    print "Can't import sympy. But probably sympy is not used."

import matplotlib.pyplot as plt


#TODO: import only when needed? (slow import of this module)
from mpl_toolkits.mplot3d import Axes3D
#from mayavi import mlab

from contracts import contract, new_contract
from mainutils import *

try:
    from sympy import Point3D
except:
    #Wrong version of sympy
    #TODO: check toNumpy
    Point3D = None




def toNumpy(obj):
    if (Point3D!=None) and (isinstance(obj, Point3D)):
        return point3DToNumpy(obj)
    else:
        return tupleToNumpy(obj)

def totuple(a):
    #http://stackoverflow.com/questions/10016352/convert-numpy-array-to-tuple
    #is this needed?
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a

def asListOfTuples(a):
    """ Return iterable as list of tuples.
        >>> asListOfTuples([np.array([1, 2]), np.array([3, 4])])
        [(1, 2), (3, 4)]
    """
    result = []

    for element in a:
        result.append(tuple(element)) #totuple

    return result

def numpyToSet(array):
    """ Convert numpy array (2D) to set of tuples.
    """
    return set(asListOfTuples(array))

def point3DToNumpy(point3d):
    #symToNumpy
    #point3DtoNumpy
    return np.array([point3d.x, point3d.y, point3d.z]).astype(np.float64)

def tupleToNumpy(tuple):
    return np.array(tuple).astype(np.float64)


norm = np.linalg.norm

def arrayNorm(x):
    """ Returns an L2-norm of vector.
    """
    return np.sum(np.abs(x)**2,axis=-1)**(1./2)

def normalize(v):
    """ Normalize vector to have norm=1.

        :param v: input vector (as numpy array or as list)
    """
    norm = np.linalg.norm(v)
    if (norm==0): 
        #TODO: accuracy of computation
        return v
    return v/norm


def findRowIndex(array, rowValue):
    """ Find row with certain value.
        :param array: np.array (2d) when rowValue will be searched
        :param rowValue: np.array - value of row
        :returns: indexes of rows containing this value (as np.array)
    """
    #Why this is tuple and i must get first value?
    return np.where((array == rowValue).all(axis=1))[0]

#TODO: multiple plots on one figure
#We need class (singleton?) - one plot for multiple geometry classes
#We can give plotter as parameter to plot function - no singleton
# class GeometryPlotter2D:
#     def __init__(self):
#         self.figure = plt.figure()
#         self.ax = fig.add_subplot(111)

#     #TODO: how to add plot? self.ax.plot?
#     def show():
#         self.ax.show()


def scatter3d(xCoord, yCoord, zCoord):
    """ Plot simple 3D scatter plot.
        Parameters are coordinates as in matplotlib.plot.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    all_same( map(len ,(xCoord, yCoord, zCoord)) )

    n = len(xCoord)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    xs, ys, zs = xCoord, yCoord, zCoord
    #zs = randrange(n, zl, zh)
    #ax.scatter(xs, ys, zs, c=c, marker=m)
    #size of markers
    # s = [0 for i in range(n)]
    # s[n-1] = 10

    ax.scatter(xs, ys, zs, c='b')


class MatplotlibPlotter(object):
    def __init__(self, show=False):
        self.ax = None
        self.plt = None
        self._show = show

    def initFigure3d(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')
        #self.ax = Axes3D(self.fig)
        ax = self.ax

        #n = len(xCoord)

        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        if not self._show: self.fig.show() #(block=False)

    def clf(self):
        if self.ax:
            self.fig.clf()
            self.ax = None

        if self.plt:
            #2D plot?
            self.fig.clf()

    def plot3d(self, xCoord, yCoord, zCoord, show=None, **kwargs):
        """ Plot 3d plot (now only simple 3D (scatter?) plot).
            Parameters are coordinates as in matplotlib.plot.
        """
        coords = [xCoord, yCoord, zCoord]

        coords = map(lambda x: list(x) + [x[0]], coords)
        print coords
        verts = np.array(coords).T

        if show==None:
            show = self._show

        if self.ax == None:
            self.initFigure3d()

        xs, ys, zs = xCoord, yCoord, zCoord
        #self.ax.plot(xs, ys, zs, c='b')
        from mpl_toolkits.mplot3d.art3d import Poly3DCollection
        poly = Poly3DCollection([verts])
        self.ax.add_collection3d(poly)

        if show==False:
            #Is this good?
            #self.fig.draw()
            self.fig.show()
            pass
        else:
            self.fig.draw()

    def show(self):
        pass
        self.fig.show()


def plotThread(plotsQueue):
    while True:
        print "plotThread"
        log.info("plotThread")
        data = plotsQueue.get()
        coords = data['coords']
        triangles = [(0,1,2)]
        s = mlab.triangular_mesh(coords[0], coords[1], coords[2], triangles, opacity=0.7)


class MayaviPlotter(object):
    def __init__(self, show=False):
        from mayavi import mlab #Will this work
        self._show = show
        self.plotThread = None
        mlab.figure(1, bgcolor=(0, 0, 0), size=(1300, 700))

    def initFigure3d(self):
        pass


    def plot3d(self, xCoord, yCoord, zCoord, color=None, polygon=None, plotType=None, show=None):
        from mayavi import mlab
        log.info("plotMayavi")
        #What to plot with this method?
        #Some primitive objects - surface, chain?
        coords = [xCoord, yCoord, zCoord]
        #triangles = Polygon3d(np.array(coords).T)
        #Maybe triangles argument instead of polygon?
        if color==None:
            color = 'b'

        #TODO: when convert colors?
        if color=='b':
            color = (0, 0, 1)
        elif color=='r':
            color = (1, 0, 0)
        else:
            pass

        #TODO: now plots only first triangle of polygon
        #  needed triangulation of polygon
        kwargs2 = {} #Mayavi don't accept invalid keywords

        # from Queue import Queue
        # from threading import Thread
        #
        # if self.plotThread==None:
        #     self.queue = Queue()
        #     self.plotThread = Thread(target=plotThread, args=(self.queue,))
        #     self.plotThread.start()
        #
        # self.queue.put({'coords':coords})
        if plotType=='line':
            mlab.plot3d(*coords, color=color)
        else:
            #plot type - surface
            #it seems to work
            #maybe automatically triangulate chain/polygon in this method?
            log.info("Plot triangles of polygon: {}".format(polygon))
            if polygon==None:
                triangles = [(0, 1, 2)]
            else:
                triangles = polygon.triangulate()
            s = mlab.triangular_mesh(coords[0], coords[1], coords[2], triangles, opacity=0.7, color=color, **kwargs2)
        #raw_input()

    def show(self):
        from mayavi import mlab
        #mlab.show(block=False)
        mlab.show()
        #mlab.draw()
        pass


def plot3d(xCoord, yCoord, zCoord):
    """ Plot simple 3D scatter plot.
        Parameters are coordinates as in matplotlib.plot.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    all_same( map(len ,(xCoord, yCoord, zCoord)) )

    n = len(xCoord)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    xs, ys, zs = xCoord, yCoord, zCoord
    #zs = randrange(n, zl, zh)
    #ax.scatter(xs, ys, zs, c=c, marker=m)
    #size of markers
    # s = [0 for i in range(n)]
    # s[n-1] = 10

    ax.plot(xs, ys, zs, c='b')

class ContainerGeometry():
    """ Container for list of geometry sets.
    """
    #Should it be possible to get elements from this set? Maybe yes?
    #Is this needed?
    #Moze to jest wazne, czy w srodku sa wielokaty czy punkty?
    #Od tego zalezy optymalny sposob przetwarzania byc moze

    def __init__(self, geomList=None, compact=False, plotter=None, color=None, conf={}):
        """ Create container geometry from list of geometry.
            :param geomList: list of geometry
            :param compact: if True, try to compact list to simpler geometry set
            :param conf: plot configuration as dict from element to key-value parameters.
        """
        if geomList==None:
            geomList=[]
        self.geomList = geomList
        self.plotter = plotter
        self.color = color
        self.conf = conf

    #TODO: change configuration; add new element with configuration

    def __len__(self):
        return len(self.geomList)

    def setPlotter(self, plt):
        self.plt = plt

    # def __iter__(self):
    #     return (char.upper() for char in text)

    def __getitem__(self, item):
        return self.geomList[item]

    def intersectionList(self, geometry2):
        for geomElement in self.geomList:
            yield geomElement.intersection(geometry2)

    def intersection(self, geometry2):
        return ContainerGeometry(self.intersectionList(geometry2))

    def sum(self, geometry2):
        self.geomList.append(geometry2)

    def plot(self, plotter=None):
        if plotter==None:
            plotter=self.plotter
        try:
            for geomElement in self.geomList:
                try:
                    #Different color per element
                    color = self.conf[geomElement]['color']
                except KeyError:
                    color = self.color
                geomElement.plot(plotter=self.plotter, color=color)
        except Exception as e:
            print(e)

    #def compact(self)

    def asList(self):
        return self.geomList


#TODO: singleton?
class EmptyGeometry:
    """ Empty Geometry. Has 0 points.
    """

emptyGeometry = EmptyGeometry()



# class Polyhedron:
#     """ 3D Polyhedron class.
#     """
#     def vertices():
#         pass

#     def faces():
#         pass

#     def inside(point):
#         pass

    #def volume():
    #   pass




class Polyline:
    """ Polygonal chain from multiple line segments (2D?).
    Contains of vertices and lines which must be consistent.

    >>> vts = [[0, 0], [0, 1], [1, 1], [1, 0]]
    >>> vts = map(np.array, vts)
    >>> pl = Polyline(vts)
    >>> np.array_equal(pl(1.5), np.array([0.5, 1]))
    True
    >>> pl.length()
    4.0
    """
    def __init__(self, vertices):
        self.vertices = vertices

        #maybe self.vertices.cyclePairwise?
        nextVertices = vertices[1:]
        nextVertices.append(vertices[0])
        tmpLines = zip(vertices, nextVertices)
        #print tmpLines
        #TODO: maybe property (for mutable) or lazy (for immutable)
        lineSegments = map(lambda (x,y): LineSegment(x,y), tmpLines)
        self.lines = lineSegments #For square - 4 line segments

    def __str__(self):
        return "[{}]".format(self.vertices)

    def length(self):
        lengths = map(lambda x: x.length(), self.lines)
        return sum(lengths)
        pass

    def __call__(self, x):
        segmentNr = 0
        sumLen = 0

        #find segment, where is x
        while (sumLen < x): #FIXME: is this good?
            sumLen += self.lines[segmentNr].length()
            segmentNr += 1

        #one step backwards
        segmentNr -= 1
        sumLen = sumLen - self.lines[segmentNr].length() 
        #segmentNr += 1

        #x from perspective of segmentNr line segment
        segmentX = x - sumLen


        return self.lines[segmentNr](segmentX)

    def plot(self, show=False, color=None, plotter=None):
        if plotter==None:
            raise NotImplementedError
        else:
            #TODO: closed chain?
            coords = np.array(self.vertices).T
            plotter.plot3d(*coords, plotType='line', show=show, color=color)


#What with performance?
#Profiler?

class ParametricCurve:
    """ Abstract parametric curve.
        Need methods length() and __call__(self, t).
    """

    def randomPoints(self, ct=20):
        pts = []

        for i in range(ct):
            x = random.random() * self.length()
            pts.append(self(x))

        return pts


class Circle(ParametricCurve):
    """ Parametric circle function.

    >>> c = Circle(2, [1, 1])
    >>> np.allclose(c(pi), np.array([-1, 1]))
    True
    >>> c.length() == 4 * pi
    True
    >>> np.allclose(c(0), np.array([3, 1]))
    True
    >>> len(c.randomPoints(10))
    10
    """
    @signature(Any, float, np.array)
    @autoassign
    def __init__(self, radius, center=[0,0]):
        pass

    def length(self):
        return 2 * pi * self.radius

    def __call__(self, t):
        """ Return point at x angle in radians counterclockwise. Starts from [1,0].
        """
        #x^2 + y^2 = R^2
        #Maybe numpy array?
        #Move circle to a vector self.center
        return self.center + [self.radius*cos(t), self.radius*sin(t)]

    #np.allclose(c(2.5*pi), np.array([1, 3]))
    #True


class Point(object):
    """ 2d Point type. Use np.array when methods not needed.
    """
    @contract(arr='array[2]|tuple[2]|list[2]')
    def __init__(self, arr):
        """ Initialize Point.
        :param arr: coordinates as np.array
        """
        #if not isinstance(arr, np.ndarray):
        #    raise TypeError("Wrong type in Point init")
        self.pt = arr

    def __eq__(self, other):
        if isinstance(other, Point):
            checkAllclose(self.pt, other.pt)
            return np.allclose(self.pt, other.pt)
        elif isinstance(other, np.ndarray):
            return np.allclose(self.pt, other)
        else:
            return False

    def __hash__(self):
        #Is this good?
        return hash((round(self.pt[0], 4), round(self.pt[1], 4)))


    def plot(self, show=False, plotter=None, color=None):
        kwargs = {}

        if color!=None:
            kwargs['color'] = color

        #TODO: exception - scatter() argument after * must be a sequence, not NoneType
        if plotter==None:
            plt.scatter(self.pt[0], self.pt[1], **kwargs)
        else:
            plotter.scatter(self.pt[0], self.pt[1], **kwargs)

        if show: plt.show()


#Bede korzystal z numpy

class LineSegment(ParametricCurve):
    """ Line Segment class. 
        Only 2D?
    """
    
    #@signature(Any, np.array, np.array)
    @autoassign
    def __init__(self, start, end):
        """
            :param start: start of line segment- Numpy array (2D vector)
            :param end: end of line segment - Numpy array (2D vector)

        >>> line = LineSegment(np.array([0, 1]), np.array([1, 0]))
        >>> np.array_equal(line.move(), np.array([1, -1]))
        True
        >>> np.array_equal(line(sqrt(2)/2), np.array([0.5, 0.5]))
        True
        >>> line.length() == sqrt(2)
        True
        >>> line2 = LineSegment(np.array([0, 0]), np.array([0.6, 0.6]))
        >>> line2.crosses(line)
        True
        >>> line.crosses(line2)
        True
        >>> line3 = LineSegment(np.array([0, 0]), np.array([0.4, 0.4]))
        >>> line.crosses(line3)
        False
        >>> line.lineEquation
        (-1.0, 1.0)
        >>> line4 = LineSegment.create([0, 0], [0, 1])
        >>> line4.lineEquation
        Traceback: (most recent call last):
        ...
        >>> pt = line.intersection(line2)
        >>> np.array_equal(pt, np.array([0.5, 0.5]))
        True
        >>> isinstance(line.intersection(line3), EmptyGeometry)
        True
        >>> line5 = LineSegment.create([-1, 3], [1, 1])
        >>> str(line5)
        'LineSegment([-1  3], [1 1])'
        >>> line5.lineEquation
        (-1.0, 2.0)
        >>> line6 = LineSegment.create([0, 0], [4, 4])
        >>> line6.lineEquation
        (1.0, 0.0)
        >>> line7 = LineSegment.create([-0.5, 0.5], [2, 0.5])
        >>> line8 = LineSegment.create("-5 10 5 -5")
        >>> str(line8)
        'LineSegment([ -5.  10.], [ 5. -5.])'
        >>> line8.lineEquation == (-1.5, 2.5)
        True
        >>> np.allclose(line8.intersection(line6), np.array([1, 1]))
        True
        >>> isinstance(line5.intersection(line7), EmptyGeometry)
        True
        >>> pt = line6.intersection(line7)
        >>> np.array_equal(pt, np.array([0.5, 0.5]))
        True
        >>>
        # >>> plt.hold(True)
        # >>> line.plot()
        # >>> line3.plot()
        """
        #if isinstance(start, np.array):
        #   self.start = start
        #else:
        #   self.start = np.array(start)

        pass

    #TODO: refactor tests (unittest, change order etc.) ?
    #TODO: more tests for intersection
    #print LineSegment(np.array([0, 1]), np.array([1, 0])).move()

    #TODO: accuracy

#   def containsPoint(self, point):
#       if self

    #TODO: check input for constructor (maybe contract or signature)

    @classmethod
    def create(klass, arg1, arg2=None):
        """ Create LineSegment from intuitive and simple to write parameters.
        This method may not be fast.
        :param arg1: start point as list or tuple or String of four numbers
        :param end: end point as list or tuple, or None if first parameter is String
        :return: Created LineSegment
        """
        #TODO: how to name this method?
        if isinstance(arg1, str):
            args = groupElements(map(float, arg1.split()), 2)
            start = args[0]
            end = args[1]
        else:
            start, end = arg1, arg2

        return klass(np.array(start), np.array(end))

    def __call__(self, x):
        return self.start + self.move()*x/self.length()

    def __repr__(self):
        #return str(self)
        return "LineSegment({0}, {1})".format(self.start, self.end)

    def __str__(self):
        #return "LineSegment({0}, {1})".format(self.start, self.end)
        return "({0}, {1})".format(self.start, self.end)

    def __hash__(self):
        return hash((totuple(self.start), totuple(self.end)))

    @property
    def points(self):
        """ Property points - start and end of line
        """
        return [self.start, self.end]

    def move(self):
        return self.end - self.start

    def length(self):
        return np.linalg.norm(self.move())

    def plot(self, show=False, plotter=None, color=None):
        #log.info("plot line - {}".format(self))

        kwargs = {}
        if color!=None:
            kwargs['color'] = color

        #TODO: Polyline
        plotCrd = np.array([self.start, self.end]).T

        if plotter==None:
            plt.plot(*plotCrd, **kwargs)
        else:
            plotter.plot(*plotCrd, **kwargs)

        if show==True:
            plt.show()

    @property
    def lineEquation(self):
        """ Return line (a*x+b) equation in form of tuple.
        :return: tuple with line equation parameters as floats - (a, b)
        """
        v = self.end - self.start

        a = float(v[1])/v[0]
        #TODO: vertical line exception
        b = self.start[1] - self.start[0]*a

        return (a, b)

    def valueForX(self, x):
        (a, b) = self.lineEquation
        return a*x + b

    #@log_rec
    def intersection(self, segment2):
        log.info((self, str(segment2)))
        if not self.doIntersects(segment2):
            return emptyGeometry

        else:
            #Compute intersection from formula
            #matrix1 = [self.start, self.end]
            #matrix2 = [segment2.start, segment2.end]
            #FIXME: some wrong results

            #Use second formula
            (a, c) = self.lineEquation
            (b, d) = segment2.lineEquation

            p = (d-c)/(a-b)

            crossPt = np.array([p, a*p+c])

            return crossPt


    def doIntersects(self, segment2):
        return self.crosses(segment2)

    def crosses(self, segment2):
        """ Return True if segment2 crosses this segment.
            If points from second segment are on different sides of line defined by first segment
            and point from first segment are on different sides of line defined by second segment
            then segments should cross themself.
        """
        #print(self, segment2)
        #ad + bc > ab + cd - maybe check this method?
        #TODO: maybe sameSide function
        b1 = orient(self.start, self.end, segment2.start)
        b2 = orient(self.start, self.end, segment2.end)
        #TODO: test if 4 points lie on the same line.

        b3 = orient(segment2.start, segment2.end, self.start)
        b4 = orient(segment2.start, segment2.end, self.end)
        #print (b1, b2, b3, b4)
        #FIXME

        #xor - ^
        if (b1 != b2) and (b3 != b4):
            return True
        else:
            return False




#print LineSegment(np.array([0, 1]), np.array([1, 0])).move()


def orient(vPrev, v, vNext):
    """ Return True if (vPrev, v, vNext) is counterclockwise.
        Should input be np.arrays?

        >>> orient([0, 0], [0, 1], [-2, 0])
        True
        >>> orient([5, 0], [-1, 6], [2.5, 2.6])
        False
        >>>
        #>>> orient([5, 0], [-1, 6], [2.5, 2.5])
    """
    #TODO: c predicates orientation
    #TODO: refactor
    matrix = [[vPrev[0], vPrev[1], 1], 
            [v[0], v[1], 1],
            [vNext[0], vNext[1], 1]]

    #print "Matrix:", matrix
    det = np.linalg.det(matrix)
    #(sign, logdet) = np.linalg.det(np.det(matrix))
    if det>0:
        return True
    elif det<0:
        return False
    else:
        raise Exception("Matrix determinant: 0")


def angle(first):
    """ Return angle of 2d vector ([x, y] pair) (counterclockwise from x axis).
        From 0 to 2*PI.
    >>> angle([1, 0])
    0.0
    >>> angle([0, 1]) == pi/2
    True
    >>> angle([-2, -2]) == pi * 5/4
    True
    """
    #FIXME: wrong angle for III and IV quandrant.
    ang = atan2(first[1],first[0])
    if ang < 0:
        ang += 2*pi

    return ang


def angleBetween(first, second):
    """ Return angle between two 2d vectors ([x, y] pair).
    >>> angleBetween([1, 1], [4, -4]) == 2*pi - pi/2
    True
    >>> angleBetween([-1, 0], [0, -1]) == pi/2
    True
    >>> angleBetween([1, -1], [1, 0]) == pi/4
    True
    """
    #if second==None:
        #angle between x axis (counterclockwise)
    #   
    #TODO: change implementation
    #print ("angleBetween", first, angle(second), angle(first))
    result = angle(second) - angle(first)
    if result < 0:
        result += 2*pi

    return result

#numpy with checkAllclose; np.allclose = ... ?

def checkAllclose(arr1, arr2):
    cp1 = np.allclose(arr1, arr2, atol=1e-15, rtol=1e-11)
    cp2 = np.allclose(arr1, arr2, atol=1e-08, rtol=1e-05)
    if cp1 != cp2:
        log.warning("np.allclose problematic results for: {}, {}".format(arr1, arr2))



class GeomReader:
    @staticmethod
    def readPointLists2D(path):
        """ Get geometrical objects in special format.
            First number of point groups(datasets) (n). Next one empty line.
            Next n point lists (divided by empty lines) containing:
                first line - number of points
                other lines - points coordinates as pair of numbers
            :param path: Path to file
            :returns: list of n groups; each group are list of 2D points
            >>> polygon = GeomReader.readPointLists2D("./tests/polygon1.dat")
            >>> len(polygon.vertices)
            5
        """
        #works

        groups = []

        #print path

        with open(path, "r") as f:
            #text = f.read()
            line = next(f)
            groupsCt = int(line)


            for i in xrange(groupsCt):
                next(f) #empty line

                pts = []
                groups.append(pts)

                pointCt = int(next(f))

                for j in xrange(pointCt):
                    line = next(f) #read line
                    (x, y) = line.split()
                    pts.append([float(x), float(y)])


        return groups

class GeomWriter:
    #TODO: append point group to file

    @staticmethod
    def writePointLists2D(groups, path):
        """ Write geometrical objects in special format.
            First number of point groups(datasets) (n). Next one empty line.
            Next n point lists (divided by empty lines) containing:
                first line - number of points
                other lines - points coordinates as pair of numbers
            :param path: Path to file
            :returns: list of n groups; each group are list of 2D points
            >>> polygon = Polygon([[0, 0], [1, 0], [1, 1], [0, 1]])
            >>> GeomWriter.writePointLists2D(polygon, "./tests/polygonTmp.dat")
            >>> polygon2 = GeomReader.readPointLists2D("./tests/polygonTmp.dat")
            >>> len(polygon2)
            4
        """
        #works
        #Structure similar to read method
        with open(path, "w+") as f:
            #text = f.read()
            groupsCt = len(groups)
            f.write("{0}\n".format(groupsCt))

            for i in xrange(groupsCt):
                pts = groups[i]

                pointCt = len(pts)

                f.write("\n{0}\n".format(pointCt))

                for j in xrange(pointCt):
                    pt = pts[j]
                    f.write("{0} {1}\n".format(pt[0], pt[1]))


        return groups


class EmptyVisualization(object):
    #Maybe without series?
    def add(self, element, series="default"):
        pass

    def mark(self, element, color='r'):
        pass

    def unmark(self, element):
        pass

    def remove(self, element, series="default"):
        pass


class Grid:
    """ Class representing multidimensional cartesian grid.

        >>> g = Grid(np.array([1,1]), np.array([2, 2]))
        >>> np.allclose(g.get(np.array([0,0])), [1,1])
        True
        >>> np.allclose(g.get(np.array([-1,0])), [-1,1])
        True
        >>> np.allclose(g.fromPosition(np.array([1.5,3.5])), [0,1])
        True
    """

    #TODO: to geomutils
    def __init__(self, start, step):
        """ Initialize grid.
            Grid is left-closed, right-opened.
            :param start: start (place of (0,0,...) of grid as np.array
            :param step: multidimensional step of grid as np.array
        """
        self.start = np.array(start)
        self.step = np.array(step)
    def get(self, element):
        """ Get physical position of element in grid. """
        return self.start + self.step * element

    def fromPosition(self, position):
        """ Return element number as np.array.
            :param position: position in this element (as np.array) """

        localPosition = position - self.start
        return localPosition // self.step

    def fromPositions(self, positions):
        localPositions = positions - self.start
        #print localPositions // self.step
        return localPositions // self.step



class NumpyEncoder(json.JSONEncoder):
    #Class from: http://stackoverflow.com/questions/3488934/simplejson-and-numpy-array/24375113#24375113

    def default(self, obj):
        """If input object is an ndarray it will be converted into a dict
        holding dtype, shape and the data, base64 encoded.
        """
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert(cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__=data_b64,
                        dtype=str(obj.dtype),
                        shape=obj.shape)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder(self, obj)

    @staticmethod
    def json_numpy_obj_hook(dct):
        """Decodes a previously encoded numpy ndarray with proper shape and dtype.

        :param dct: (dict) json encoded ndarray
        :return: (ndarray) if input was an encoded ndarray
        """
        if isinstance(dct, dict) and '__ndarray__' in dct:
            data = base64.b64decode(dct['__ndarray__'])
            return np.frombuffer(data, dct['dtype']).reshape(dct['shape'])
        return dct

    @staticmethod
    def testNumpyEncoder():
        expected = np.arange(100, dtype=np.float)
        dumped = json.dumps(expected, cls=NumpyEncoder)
        result = json.loads(dumped, object_hook=json_numpy_obj_hook)


        # None of the following assertions will be broken.
        assert result.dtype == expected.dtype, "Wrong Type"
        assert result.shape == expected.shape, "Wrong Shape"
        assert np.allclose(expected, result), "Wrong Values"





if __name__ == "__main__":
    import doctest
    doctest.testmod()
